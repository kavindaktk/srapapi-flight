<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';
require 'class/scrapme.php';
require 'class/database.php';

$app = new \Slim\App;
$app->get('/scrap', function (Request $request, Response $response) {
//http://localhost/scrapAPI/public/scrap?format=csc&city=colombo&state=srilanka&country=srilanka&checkin=2017-08-01&checkout=2017-08-03&people=2  $format = $_GET['format'];
	
	 
   //initiate PDO connection
    $pdoConnection = new database();
   // initiate scrapper class 
	$scrapper = new scrapme($pdoConnection);
	
	// get request format
	$format = $_GET['format'];
	
	if ($format == "csc"){
		if(isset($_GET['city']) AND isset($_GET['state']) AND isset($_GET['country']) AND isset($_GET['checkin']) AND isset($_GET['checkout']) AND isset($_GET['people'])){
			$city = $_GET['city'];
			$state = $_GET['state'];
			$country = $_GET['country'];
			$IsAllPramSet = true;
			$checkin = $_GET['checkin'];
			$checkout = $_GET['checkout'];
			$people = $_GET['people'];
			
			$location = $city.", ".$state.", ".$country;
			$result = $scrapper->scrapURL($location,$checkin,$checkout,1,$people,0);

		} 
		else {$result = "Check request parameters";}
	} else if($format == "cc"){
		if(isset($_GET['city']) AND isset($_GET['country']) AND isset($_GET['checkin']) AND isset($_GET['checkout']) AND isset($_GET['people'])){
			$city = $_GET['city'];
			$country = $_GET['country'];
			$IsAllPramSet = true;
			$checkin = $_GET['checkin'];
			$checkout = $_GET['checkout'];
			$people = $_GET['people'];
			
			$location = $city.", ".$country;
			$result = $scrapper->scrapURL($location,$checkin,$checkout,1,$people,0);

		} 
		else {$result = "Check request parameters";}
	}
	

   
   
       $data = array('Status' => $result);
       $newResponse = $response->withJson($data, 200);
       return  $newResponse;
});

  
  $app->get('/flightscrap', function (Request $request, Response $response) {
	  
	  //http://localhost/scrapAPI/public/flightscrap?fromCountry="srilanka%20colombo"&toCountry="Miami united State America"&month=08&day=10&year2017&ReturnFromCountry=miami united state america&ReturnToCountry=srilanka colombo&returnCountryCode=cmb&returnMonth=08&returnDay=20&returnYear=2017
	  if(isset($_GET['fromCountry']) AND isset($_GET['toCountry']) AND isset($_GET['month']) 
			AND isset($_GET['day']) AND isset($_GET['year'])
			AND isset($_GET['ReturnFromCountry'])
			AND isset($_GET['ReturnToCountry'])
			AND isset($_GET['returnCountryCode'])
			AND isset($_GET['returnMonth'])
			AND isset($_GET['returnDay'])
			AND isset($_GET['returnYear'])		
			){ 
	  $fromCountry = $_GET['fromCountry'];
	  $toCountry = $_GET['toCountry'];
	  $month = $_GET['month'];
	  $day = $_GET['day'];
	  $year = $_GET['year'];
	  $ReturnFromCountry = $_GET['ReturnFromCountry'];
	  $ReturnToCountry = $_GET['ReturnToCountry'];
	  $returnCountryCode = $_GET['returnCountryCode'];
	  $returnMonth = $_GET['returnMonth'];
	  $returnDay = $_GET['returnDay'];
	  $returnYear = $_GET['returnYear'];
	  
			} else {$result = "Check request parameters";}
	  
	   //initiate PDO connection
    $pdoConnection = new database();
   // initiate scrapper class 
	$scrapper = new scrapme($pdoConnection);
	
	echo $result = $scrapper->FlightsScrapURL($fromCountry,$toCountry,$month,$day,$year,$ReturnFromCountry,$ReturnToCountry,$returnCountryCode,$returnMonth,$returnDay,$returnYear);
	  
	       $data = array('Status' => $result);
       $newResponse = $response->withJson($data, 200);
       return  $newResponse;
	   
	  });
  
$app->run();


?>